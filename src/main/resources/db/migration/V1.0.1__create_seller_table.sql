CREATE TABLE IF NOT EXISTS sellers (
    id serial primary key,
    first_name varchar(255) not null,
    last_name varchar(255),
    personal_address varchar(255),
    personal_email varchar(255),
    personal_phone_number varchar(255),
    is_organization boolean default false,
    company_name varchar(255),
    date_of_birth timestamp not null,
    created_at timestamp,
    updated_at timestamp
);

alter table products add unique (code);