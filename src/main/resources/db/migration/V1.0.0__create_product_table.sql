CREATE TABLE IF NOT EXISTS products (
    id serial primary key,
    code varchar(10),
    name varchar(255),
    is_active bool default true,
    seller_id int not null,
    quantity int default 0,
    has_expiration boolean default false,
    expired_date timestamp,
    weight numeric(10,3),
    weight_unit varchar(255),
    created_at timestamp,
    updated_at timestamp
);