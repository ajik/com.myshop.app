package com.myshop.app.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.sql.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity(name = "merchants")
public class Merchant extends BaseModel {

    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "personal_address")
    private String personalAddress;
    @Column(name = "personal_email")
    private String personalEmail;
    @Column(name = "personal_phone_number")
    private String personalPhoneNumber;
    @Column(name = "is_organization")
    private boolean isOrganization;
    @Column(name = "company_name")
    private String companyName;
    @Column(name = "date_of_birth")
    private Date dateOfBirth;
}
