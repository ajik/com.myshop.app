package com.myshop.app.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.sql.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity(name = "products")
public class Product extends BaseModel {

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "is_active")
    private boolean isActive;

    @Column(name = "seller_id")
    private Integer sellerId;

    @Column(name = "quantity")
    private long quantity;

    @Column(name = "has_expiration")
    private Boolean hasExpiration;

    @Column(name = "expired_date")
    private Date expiredDate;

    @Column(name = "weight")
    private float weight;

    @Column(name = "weight_unit")
    private String weightUnit;

}
