package com.myshop.app.repository;

import com.myshop.app.model.Merchant;

public interface MerchantRepository extends BaseRepository<Merchant> {
}
