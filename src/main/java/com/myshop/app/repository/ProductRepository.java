package com.myshop.app.repository;

import com.myshop.app.model.Product;

public interface ProductRepository extends BaseRepository<Product> { }
