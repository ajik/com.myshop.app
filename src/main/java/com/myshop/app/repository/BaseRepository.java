package com.myshop.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.lang.NonNull;

import java.util.List;

@NoRepositoryBean
public interface BaseRepository<T> extends CrudRepository<T, Integer> {
    @NonNull List<T> findAll();
}
