package com.myshop.app.service;

import com.myshop.app.model.Merchant;

public interface MerchantService extends BaseService<Merchant> { }
