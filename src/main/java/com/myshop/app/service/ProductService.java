package com.myshop.app.service;

import com.myshop.app.model.Product;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductService extends BaseService<Product>{ }
