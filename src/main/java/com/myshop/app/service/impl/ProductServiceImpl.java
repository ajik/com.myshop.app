package com.myshop.app.service.impl;

import com.myshop.app.model.Product;
import com.myshop.app.repository.ProductRepository;
import com.myshop.app.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class ProductServiceImpl implements ProductService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Product findById(Integer id) {
        return productRepository.findById(id).orElse(null);
    }

    @Override
    public Product insert(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product updateById(Integer id, Product product) throws Exception {
        Product existingProduct = productRepository.findById(id).orElse(null);
        if(Objects.isNull(existingProduct)) {
            throw new Exception("Data is not exist");
        }

        existingProduct.setName(product.getName());
        existingProduct.setHasExpiration(product.getHasExpiration());
        if(existingProduct.getHasExpiration()) {
            existingProduct.setExpiredDate(Objects.isNull(product.getExpiredDate()) ? existingProduct.getExpiredDate() : product.getExpiredDate());
        } else {
            existingProduct.setExpiredDate(null);
        }
        existingProduct.setQuantity(product.getQuantity());
        existingProduct.setSellerId(product.getSellerId());
        existingProduct.setWeight(product.getWeight());
        existingProduct.setWeightUnit(product.getWeightUnit());

        return productRepository.save(existingProduct);
    }

    @Override
    public void deleteById(Integer id) {
        productRepository.deleteById(id);
    }
}
