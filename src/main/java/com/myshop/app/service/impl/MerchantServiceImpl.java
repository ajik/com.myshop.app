package com.myshop.app.service.impl;

import com.myshop.app.model.Merchant;
import com.myshop.app.repository.MerchantRepository;
import com.myshop.app.service.MerchantService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MerchantServiceImpl implements MerchantService {

    private final MerchantRepository merchantRepository;

    public MerchantServiceImpl(MerchantRepository sellerRepository) {
        this.merchantRepository = sellerRepository;
    }

    @Override
    public List<Merchant> findAll() {
        return merchantRepository.findAll();
    }

    @Override
    public Merchant findById(Integer id) {
        return merchantRepository.findById(id).orElse(null);
    }

    @Override
    public Merchant insert(Merchant merchant) {
        return merchantRepository.save(merchant);
    }

    @Override
    public Merchant updateById(Integer id, Merchant merchant) throws Exception {
        return null;
    }

    @Override
    public void deleteById(Integer id) {
        merchantRepository.deleteById(id);
    }
}
