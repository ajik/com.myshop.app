package com.myshop.app.service;

import java.util.List;

public interface BaseService<T> {
    List<T> findAll();
    T findById(Integer id);
    T insert(T t);
    T updateById(Integer id, T t) throws Exception;
    void deleteById(Integer id);
}
