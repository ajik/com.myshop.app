package com.myshop.app.utils.generator;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Responses<T> {

    private String status;
    private String message;
    private T data;

}
