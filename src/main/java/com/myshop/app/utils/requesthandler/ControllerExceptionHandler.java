package com.myshop.app.utils.requesthandler;

import com.myshop.app.utils.generator.Responses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Arrays;

import static com.myshop.app.utils.enums.ResponseStatus.ERROR;

@RestControllerAdvice
public class ControllerExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(value = { Exception.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Responses BadRequestException(Exception e) {
        logger.error("" + Arrays.toString(e.getStackTrace()));
        return new Responses(ERROR.getUpperCase(), e.getMessage(), null);
    }
}
