package com.myshop.app.utils.enums;

import lombok.Getter;

@Getter
public enum ResponseStatus {

    SUCCESS("success", "SUCCESS"),
    ERROR("error", "ERROR");

    private final String lowerCase;
    private final String upperCase;

    ResponseStatus(String lowerCase, String upperCase) {
        this.lowerCase = lowerCase;
        this.upperCase = upperCase;
    }
}
