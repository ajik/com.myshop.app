package com.myshop.app.controller;

import com.myshop.app.model.Merchant;
import com.myshop.app.service.MerchantService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/merchants")
public class MerchantController extends BaseController<Merchant, MerchantService> {

    public MerchantController(MerchantService merchantService) {
        super(merchantService);
    }
}
