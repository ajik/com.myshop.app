package com.myshop.app.controller;

import com.myshop.app.model.Product;
import com.myshop.app.service.ProductService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/products")
public class ProductController extends BaseController<Product, ProductService> {

    public ProductController(ProductService productService) {
        super(productService);
    }
}
