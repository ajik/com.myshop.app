package com.myshop.app.controller;

import com.myshop.app.model.Product;
import com.myshop.app.service.BaseService;
import com.myshop.app.utils.generator.Responses;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

import static com.myshop.app.utils.enums.ResponseStatus.SUCCESS;

@SuppressWarnings("rawtypes")
public abstract class BaseController<T, R extends BaseService> {

    private final R service;

    public BaseController(R service) {
        this.service = service;
    }

    @GetMapping
    @SuppressWarnings({"unchecked"})
    public Responses<List<Product>> get() {
        List<Product> products = service.findAll();
        return new Responses<>(SUCCESS.getUpperCase(), "", products);
    }

    @GetMapping("/{id}")
    @SuppressWarnings({"unchecked"})
    public Responses<T> getById(@PathVariable(value = "id") String idStr) {
        T t = (T) service.findById(Integer.valueOf(idStr));
        return new Responses<>(SUCCESS.getUpperCase(), "", t);
    }

    @PostMapping
    @SuppressWarnings({"unchecked"})
    public Responses<T> insert(@RequestBody T t) {
        service.insert(t);
        return new Responses<>(SUCCESS.getUpperCase(), "", t);
    }

    @PutMapping("/{id}")
    @SuppressWarnings({"unchecked"})
    public Responses<T> updateById(@RequestBody T t, @PathVariable(value = "id") String idStr) throws Exception {
        service.updateById(Integer.valueOf(idStr), t);
        return new Responses<>(SUCCESS.getUpperCase(), "", t);
    }

    @DeleteMapping("/{id}")
    public Responses<T> deleteById(@PathVariable(value = "id") String idStr) {
        service.deleteById(Integer.valueOf(idStr));
        return new Responses<>(SUCCESS.getUpperCase(), "", null);
    }
}
